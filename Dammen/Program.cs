﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Dammen {
    public enum Direction { UpLeft, UpRight, DownLeft, DownRight }

    internal static class Program {
        private static readonly Random random = new();

        /// <summary>Score board for white</summary>
        private static double ScoreBoard(Board board) {
            return
                ((random.NextDouble() - 0.5) / 100.0) +   // Tie breaker
                BitHelper.CountBits(board.WhiteStones & ~board.IsPromoted) + (3.0 * BitHelper.CountBits(board.IsPromoted & board.WhiteStones))
                - BitHelper.CountBits(board.BlackStones & ~board.IsPromoted) - (3.0 * BitHelper.CountBits(board.IsPromoted & board.BlackStones));
        }

        private static (Move bestMove, double bestScore) ScoreBoardRecursive(Board board, bool whitesTurn, bool findMax, int depth) {
            if (depth == 0) {
                return (default, ScoreBoard(board));
            } else {
                var possibleMoves = board.GetPossibleMoves(whitesTurn);
                var bestScore = findMax ? double.MinValue : double.MaxValue;
                Move bestMove = default;
                foreach (var move in possibleMoves) {
                    var score = ScoreBoardRecursive(board.Apply(move), !whitesTurn, !findMax, depth - 1).bestScore;
                    if (findMax) {
                        if (score > bestScore) {
                            bestScore = score;
                            bestMove = move;
                        }
                    } else {
                        if (score < bestScore) {
                            bestScore = score;
                            bestMove = move;
                        }
                    }
                }
                return (bestMove, bestScore);
            }
        }

        private static void Main(string[] _) {
            BitHelper.Init();
            var board = Board.Initial();
            var turns = new List<Move>();

            const bool whiteIsComputer = true;
            const bool blackIsComputer = true;

            bool whitesTurn = true;
            while (true) {
                Console.WriteLine($"*** Turn {turns.Count}");
                board.Print();

                var moves = board.GetPossibleMoves(whitesTurn);
                moves = moves.OrderBy(x => (x.FromIdx, x.ToIdx)).ToList();
                if (moves.Count == 0) {
                    break;
                }

                for (int i = 0; i < moves.Count; i++) {
                    Console.Write($"{(char)('A' + i)}.{moves[i]}  ");
                }
                Console.WriteLine();

                Move move;
                if ((whitesTurn && !whiteIsComputer) || (!whitesTurn && !blackIsComputer)) {
                    int t = -1;
                    while (t < 0 || t >= moves.Count) {
                        char ch = Console.ReadKey().KeyChar.ToString().ToUpperInvariant()[0];
                        t = ch - 'A';
                    }
                    move = moves[t];
                } else {
                    if (moves.Count == 1) {
                        move = moves[0];
                    } else {
                        Console.WriteLine("(Thinking...)");
                        var (bestMove, bestScore) = ScoreBoardRecursive(board, whitesTurn, whitesTurn, 7);
                        move = bestMove;
                        Console.WriteLine($"> {bestMove}, moveScore={bestScore})\n");
                    }
                }

                Console.WriteLine($">{move}\n");
                if (move.FromIdx == 0 && move.ToIdx == 0) {
                    break;
                }

                turns.Add(move);
                board = board.Apply(move);
                whitesTurn = !whitesTurn;
            }
        }
    }
}
