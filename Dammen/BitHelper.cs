﻿using System;
using System.Collections.Generic;

namespace Dammen {
    static public class BitHelper {
        /// <summary>Map value of bit (1L << bitIdx) back to bitIdx</summary>
        private static readonly sbyte[] lookupOneBit = { -1, 0, 1, 17, 2, 47, 18, 14, 3, 34, 48, 6, 19, 24, 15, 12, 4, 10, 35, 37, 49, 31, 7, 39, 20, 42, 25, -1, 16, 46, 13, 33, 5, 23, 11, 9, 36, 30, 38, 41, -1, 45, 32, 22, 8, 29, 40, 44, 21, 28, 43, 27, 26 };
        private static byte[] CountBitsArray;

        static public IEnumerable<int> GetBitsSet(Int64 mask) {
            while (mask != 0L) {
                var oneBit = mask & -mask;                  // Retrieve LSB
                yield return lookupOneBit[oneBit % 53];
                mask &= ~oneBit;                            // Remove LSB
            }
        }

        static private int CountBitsSlow(Int64 mask) {
            int res = 0;
            while (mask != 0L) {
                res += 1;
                mask &= ~(mask & -mask);                    // Remove LSB
            }
            return res;
        }

        static public int CountBits(Int64 mask) =>
            CountBitsArray[mask & 0xffff] +
            CountBitsArray[(mask >> 16) & 0xffff] +
            CountBitsArray[(mask >> 32) & 0xffff] +
            CountBitsArray[(mask >> 48) & 0xffff];

        static public void Init() {
            CountBitsArray = new byte[65536];
            for (int i = 0; i < 65536; i++) {
                CountBitsArray[i] = (byte)CountBitsSlow(i);
            }
        }
    }
}
