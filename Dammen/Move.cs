﻿using System;
using System.Linq;
using static Dammen.Squares;

namespace Dammen {
    /// <summary>A move of one stone from a square to another square, potentially captures stone(s)</summary>
    public struct Move {
        /// <summary>The move encoded as bitfields</summary>
        private readonly Int64 Data;

        public int FromIdx => (int)Data & 63;
        public int ToIdx => ((int)Data >> 6) & 63;
        /// <summary>The stones captured by this move</summary>
        public Int64 CaptureMask => Data >> 12;
        /// <summary>Number of stones captures by this move</summary>
        public int Captured() => BitHelper.CountBits(CaptureMask);
        public bool IsCapture() => CaptureMask != 0L;

        public override string ToString() {
            string captured = Captured() > 1
                ? "(" + string.Concat(BitHelper.GetBitsSet(CaptureMask).Select(b => $"x{b}")) + ")"
                : "";
            return $"{FromIdx}{(IsCapture() ? 'x' : '-')}{ToIdx}{captured}";
        }

        static public Move UpLeft(int idx) => new(idx, UpLeftIdx[idx], 0);
        static public Move UpRight(int idx) => new(idx, UpRightIdx[idx], 0);
        static public Move DownLeft(int idx) => new(idx, DownLeftIdx[idx], 0);
        static public Move DownRight(int idx) => new(idx, DownRightIdx[idx], 0);

        //public Move AddCapture(int capturedIdx, int newToIdx) => new(FromIdx, newToIdx, CaptureMask | (1L << capturedIdx));

        public Move(int fromIdx, Direction direction) : this(fromIdx, GetDirectionIdx(fromIdx, direction), 0) { }

        /// <summary>Constructors</summary>
        public Move(int fromIdx, int toIdx, Int64 captureMask) =>
            Data = (Int64)fromIdx | (Int64)(toIdx << 6) | (captureMask << 12);
    }
}
