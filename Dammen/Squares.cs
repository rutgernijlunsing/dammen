﻿using System;

namespace Dammen {
    /// <summary>10x10 checkers board positions, zero-based numbered from 0 (black corner) to 49 (white corner)</summary>
    public static class Squares {
        public static readonly sbyte[] UpLeftIdx, UpRightIdx, DownLeftIdx, DownRightIdx;
        private static readonly sbyte[][] DirectionIdx;

        public const Int64 FullMask = (1L << 50) - 1;
        public const Int64 topRowMask = (1L << 5) - 1;
        public const Int64 bottomRowMask = topRowMask << 45;
        private const Int64 evenRowsMask = topRowMask + (topRowMask << 10) + (topRowMask << 20) + (topRowMask << 30) + (topRowMask << 40);
        public const Int64 oddRowsMask = evenRowsMask << 5;
        private const Int64 leftColumnMask = (1L << 5) + (1L << 15) + (1L << 25) + (1L << 35) + (1L << 45);
        private const Int64 rightColumnMask = leftColumnMask >> 1;

        public const Int64 UpLeftMovePossibleMask = FullMask & ~leftColumnMask & ~topRowMask;
        public const Int64 UpRightMovePossibleMask = FullMask & ~rightColumnMask & ~topRowMask;
        public const Int64 DownLeftMovePossibleMask = FullMask & ~leftColumnMask & ~bottomRowMask;
        public const Int64 DownRightMovePossibleMask = FullMask & ~rightColumnMask & ~bottomRowMask;

        public static Int64 ShiftMaskDownRight(Int64 mask) => ((mask & DownRightMovePossibleMask & evenRowsMask) << 6) | ((mask & DownRightMovePossibleMask & oddRowsMask) << 5);
        public static Int64 ShiftMaskDownLeft(Int64 mask) => ((mask & DownLeftMovePossibleMask & evenRowsMask) << 5) | ((mask & DownLeftMovePossibleMask & oddRowsMask) << 4);
        public static Int64 ShiftMaskUpRight(Int64 mask) => ((mask & UpRightMovePossibleMask & evenRowsMask) >> 4) | ((mask & UpRightMovePossibleMask & oddRowsMask) >> 5);
        public static Int64 ShiftMaskUpLeft(Int64 mask) => ((mask & UpLeftMovePossibleMask & evenRowsMask) >> 5) | ((mask & UpLeftMovePossibleMask & oddRowsMask) >> 6);
        public static Int64 ShiftMask(Int64 mask, Direction direction) => direction switch {
            Direction.UpLeft => ShiftMaskUpLeft(mask),
            Direction.UpRight => ShiftMaskUpRight(mask),
            Direction.DownLeft => ShiftMaskDownLeft(mask),
            Direction.DownRight => ShiftMaskDownRight(mask),
            _ => throw new NotImplementedException()
        };

        /// <summary>Gets square index going 1 step into given direction. Returns -1 if off the board</summary>
        public static int GetDirectionIdx(int idx, Direction direction) => DirectionIdx[(int)direction][idx];
        /// <summary>Gets square index going 2 steps into given direction. Returns -1 if off the board</summary>
        public static int GetDirectionIdxTwice(int idx, Direction direction) => GetDirectionIdx(GetDirectionIdx(idx, direction), direction);

        static Squares() {
            static sbyte GetDirectionIdx(int idx, Int64 validMask, int deltaIdx) =>
                (validMask & (1L << idx)) != 0 ? (sbyte)(idx + deltaIdx) : (sbyte)-1;
            UpLeftIdx = new sbyte[50];
            UpRightIdx = new sbyte[50];
            DownLeftIdx = new sbyte[50];
            DownRightIdx = new sbyte[50];
            DirectionIdx = new[] { UpLeftIdx, UpRightIdx, DownLeftIdx, DownRightIdx };
            for (sbyte y = 0; y < 10; y++) {
                bool oddRow = y % 2 == 1;
                for (sbyte x = 0; x < 5; x++) {
                    var idx = (y * 5) + x;
                    UpLeftIdx[idx] = GetDirectionIdx(idx, UpLeftMovePossibleMask, oddRow ? -6 : -5);
                    UpRightIdx[idx] = GetDirectionIdx(idx, UpRightMovePossibleMask, oddRow ? -5 : -4);
                    DownLeftIdx[idx] = GetDirectionIdx(idx, DownLeftMovePossibleMask, oddRow ? +4 : +5);
                    DownRightIdx[idx] = GetDirectionIdx(idx, DownRightMovePossibleMask, oddRow ? +5 : +6);
                }
            }
        }
    }
}
