﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using static Dammen.Squares;

namespace Dammen {
    [Flags]
    public enum Stone {
        None = 0,
        Occupied = 1,
        White = 2,
        Promoted = 4
    }

    public struct Board {
        /// <summary>50-bit mask: 1 if square has a stone</summary>
        public Int64 IsOccupied;
        /// <summary>50-bit mask: 1 if square has a white stone, 0 if square has a black stone</summary>
        public Int64 IsWhite;
        /// <summary>50-bit mask: 1 if square has a promoted stone; 0 if square has non-promoted stone</summary>
        public Int64 IsPromoted;

        public Int64 WhiteStones => IsOccupied & IsWhite;
        public Int64 BlackStones => IsOccupied & ~IsWhite;

        public Board Apply(Move move) {
            var fromMask = 1L << move.FromIdx;
            Debug.Assert((IsOccupied & fromMask) != 0L, "Invalid from-move");
            var toMask = 1L << move.ToIdx;
            Debug.Assert((fromMask == toMask) || ((IsOccupied & toMask) == 0L), "Invalid to-move");
            var keepMask = ~move.CaptureMask & ~fromMask & ~toMask; // Reset all captures & reset source stone & reset dest stone
            var wasWhite = ((IsWhite >> move.FromIdx) & 1L) << move.ToIdx;
            var wasPromoted = ((IsPromoted >> move.FromIdx) & 1L) << move.ToIdx;
            var isPromoted = toMask & (wasWhite != 0L ? topRowMask : bottomRowMask);
            return new() {
                IsOccupied = (IsOccupied & keepMask) | toMask,
                IsWhite = (IsWhite & keepMask) | wasWhite,
                IsPromoted = (IsPromoted & keepMask) | wasPromoted | isPromoted
            };
        }

        public Stone GetStone(int idx) {
            var isOccupied = (IsOccupied >> idx) & 1;
            var isWhite = (IsWhite >> idx) & 1;
            var isPromoted = (IsPromoted >> idx) & 1;
            var val = isOccupied + (isWhite << 1) + (isPromoted << 2);
            return (Stone)val;
        }

        private static string StoneToString(Stone stone) => (int)stone switch {
            (int)Stone.None => "_",
            (int)(Stone.Occupied) => "b",
            (int)(Stone.Occupied | Stone.Promoted) => "B",
            (int)(Stone.Occupied | Stone.White) => "w",
            (int)(Stone.Occupied | Stone.White | Stone.Promoted) => "W",
            _ => throw new Exception(stone.ToString())
        };

        public void Print(Int64 maskLeft = 0, Int64 maskRight = 0) {
            for (int y = 0; y < 10; y++) {
                Console.Write($"{y * 5,02}: ");
                if (y % 2 == 0) Console.Write("  ");
                for (int x = 0; x < 5; x++) {
                    int idx = (y * 5) + x;
                    var str = StoneToString(GetStone(idx));
                    bool idxMaskLeft = (maskLeft & (1L << idx)) != 0;
                    bool idxMaskRight = (maskRight & (1L << idx)) != 0;
                    Console.Write($"{(idxMaskLeft ? '<' : ' ')}{str}{(idxMaskRight ? '>' : ' ')} ");
                }
                Console.WriteLine("\n");
            }
        }

        public override string ToString() {
            var b = this;
            return
                string.Concat(Enumerable.Range(0, 10).Select(y =>
                    string.Concat(Enumerable.Range(0, 5).Select(x =>
                        StoneToString(b.GetStone((y * 5) + x)))) + " | "));
        }

        /// <summary>Recursively finds all capture-moves where no additional capture could be made</summary>
        private static void CaptureMoves(Move move, Int64 occupied, Int64 capturableStonesLeft, List<Move> moves) {
            bool lastCapture = true;
            if (capturableStonesLeft != 0L) {
                for (int direction = 0; direction < 4; direction++) {
                    var capturedIdx = GetDirectionIdx(move.ToIdx, (Direction)direction);
                    var capturedMask = 1L << capturedIdx;
                    if (capturedIdx >= 0 && (capturableStonesLeft & capturedMask) != 0L) {
                        var destIdx = GetDirectionIdx(capturedIdx, (Direction)direction);
                        if (destIdx >= 0 && ((occupied & (1L << destIdx)) == 0L)) {
                            var newMove = new Move(move.FromIdx, destIdx, move.CaptureMask | capturedMask);
                            CaptureMoves(newMove, occupied, capturableStonesLeft & ~capturedMask, moves);   // Recurse
                            lastCapture = false;
                        }
                    }
                }
            }
            if (lastCapture) {
                moves.Add(move);  // No additional captures where made; store final capture-move
            }
        }

        /// <summary>Collect all straight-line moves & captures of a promoted stone</summary>
        /// <returns>Returns true if a capture move was found</returns>
        private static bool GetPossiblePromotedMoves(Move move, Int64 occupied, Int64 capturableStonesLeft, bool onlyCaptures, List<Move> moves) {
            bool foundCapture = false;
            for (int dir = 0; dir < 4; dir += 1) {
                int cur = GetDirectionIdx(move.ToIdx, (Direction)dir);
                while (cur != -1) {                             // Not at border?
                    var curMask = 1L << cur;
                    if ((occupied & curMask) != 0L) {           // Not empty?
                        bool capturable = (capturableStonesLeft & curMask) != 0L;
                        if (capturable) {                       // Not-yet-captured-this-turn opponent stone?
                            var captureMask = curMask;
                            cur = GetDirectionIdx(cur, (Direction)dir);
                            while (cur != -1) {
                                curMask = 1L << cur;
                                if ((occupied & curMask) != 0L) {
                                    break;      // Not empty after opponent stone: done
                                }
                                // Empty space after opponent stone: found a capture!
                                foundCapture = true;
                                Move newMove = new(move.FromIdx, cur, move.CaptureMask | captureMask);
                                // Recursively try to find an additional capture from here:
                                bool foundExtraCapture = GetPossiblePromotedMoves(newMove, occupied, capturableStonesLeft & ~captureMask, true, moves);
                                if (!foundExtraCapture) {
                                    moves.Add(newMove);   // No additional captures, so keep this one
                                }
                                cur = GetDirectionIdx(cur, (Direction)dir);
                            }
                        }
                        break;
                    }
                    if (!onlyCaptures) {
                        // Empty space around stone: found a move
                        moves.Add(new(move.FromIdx, cur, move.CaptureMask));
                    }
                    cur = GetDirectionIdx(cur, (Direction)dir);
                }
            }
            return foundCapture;
        }

        public List<Move> GetPossibleMoves(bool white) {
            var moves = new List<Move>();
            var ourStones = white ? WhiteStones : BlackStones;
            var otherStones = white ? BlackStones : WhiteStones;
            var free = ~IsOccupied & FullMask;
            var notOtherStones = ourStones | free;

            // Capturable stones for this turn are stones of the opponent which can be captured in a specific direction
            // since one side is empty, and the other side is either our stone or also empty:
            var downLeftCapturable = otherStones & ShiftMaskUpRight(free) & ShiftMaskDownLeft(notOtherStones);
            var downRightCapturable = otherStones & ShiftMaskUpLeft(free) & ShiftMaskDownRight(notOtherStones);
            var upLeftCapturable = otherStones & ShiftMaskDownRight(free) & ShiftMaskUpLeft(notOtherStones);
            var upRightCapturable = otherStones & ShiftMaskDownLeft(free) & ShiftMaskUpRight(notOtherStones);
            var capturable = downLeftCapturable | downRightCapturable | upLeftCapturable | upRightCapturable;

            // Find all moves & captures of our promoted stones:
            bool hasPromotionCapture = false;
            foreach (int idx in BitHelper.GetBitsSet(ourStones & IsPromoted)) {
                hasPromotionCapture =
                    GetPossiblePromotedMoves(new Move(idx, idx, 0L), IsOccupied & ~(1L << idx), capturable, false, moves) 
                    || hasPromotionCapture;
            }

            // Find all captures of non-promoted stones:
            var downLeftCaptureFrom = ShiftMaskUpRight(downLeftCapturable);
            var downRightCaptureFrom = ShiftMaskUpLeft(downRightCapturable);
            var upLeftCaptureFrom = ShiftMaskDownRight(upLeftCapturable);
            var upRightCaptureFrom = ShiftMaskDownLeft(upRightCapturable);
            var captureFrom = (downLeftCaptureFrom | downRightCaptureFrom | upLeftCaptureFrom | upRightCaptureFrom) & ourStones;
            bool hasNonPromotionCapture = captureFrom != 0L;
            if (hasNonPromotionCapture) {
                foreach (var idx in BitHelper.GetBitsSet(captureFrom)) {
                    CaptureMoves(new Move(idx, idx, 0L), IsOccupied & ~(1L << idx), capturable, moves);
                }
            }

            if (hasPromotionCapture || hasNonPromotionCapture) {        // Do we have captures?
                // Only keep moves with max. number of captures:
                if (moves.Count > 1) {
                    int maxCaptured = moves.Max(m => m.Captured());
                    moves.RemoveAll(m => m.Captured() != maxCaptured);
                }
                return moves;
            }

            // No captures found: Add the moves of the non-promoted stones:
            ourStones &= ~IsPromoted;
            if (white) {
                var leftMovePossibleMask = ourStones & ShiftMaskDownRight(free);
                foreach (int idx in BitHelper.GetBitsSet(leftMovePossibleMask)) {
                    moves.Add(Move.UpLeft(idx));
                }
                var rightMovePossibleMask = ourStones & ShiftMaskDownLeft(free);
                foreach (int idx in BitHelper.GetBitsSet(rightMovePossibleMask)) {
                    moves.Add(Move.UpRight(idx));
                }
                //Console.WriteLine($"{leftMovePossibleMask} {rightMovePossibleMask}");
            } else {
                var leftMovePossibleMask = ourStones & ShiftMaskUpRight(free);
                foreach (int idx in BitHelper.GetBitsSet(leftMovePossibleMask)) {
                    moves.Add(Move.DownLeft(idx));
                }
                var rightMovePossibleMask = ourStones & ShiftMaskUpLeft(free);
                foreach (int idx in BitHelper.GetBitsSet(rightMovePossibleMask)) {
                    moves.Add(Move.DownRight(idx));
                }
                //Console.WriteLine($"{leftMovePossibleMask} {rightMovePossibleMask}");
            }

            var board = this;
            Debug.Assert(moves.All(m => board.Apply(m).GetType() != null));  // Check validity of moves

            return moves;
        }

        static public Board Initial() {
            var res = new Board();
            const Int64 oneColorMask = (1L << 20) - 1;
            res.IsOccupied = oneColorMask | (oneColorMask << 30);
            res.IsWhite = oneColorMask << 30;
            res.IsPromoted = 0; // res.IsWhite;
            return res;
        }
    }
}
